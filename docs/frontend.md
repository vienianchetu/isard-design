# Frontend

- Primera fase: Mínim: veure escriptoris persistents i fer start i stop
    - Primero tiene que salir un bloque diferenciado que diga "Els teus escriptoris" y dónde salgan botones como los de ahora.
    - Debajo sale un bloque que diga "Escriptoris temporals" dónde salgan los no persistentes, con botones más pequeños y/o diferenciados.
        - Cal un text ben visible i clar explicant que es perden les dades guardades dins l'escriptori
    - Estaria bien que se pueda cambiar la vista de botón a tabla pero por defecto botones (o bien si no hay  demasiados)
        - Es veuen més petits els botons o tenen una altra forma
- Segona fase:
    - Screenshots com a foto del desktop
    - Quan passa el ratolí sobre el botó es veu canvi
    - Quan es clica al botó s'indica que està esperant a ser arrencat amb algo que fa voltes
    - La idea un poco es que haya el estado:
        - Parado: al clicar se pone dando vueltas hasta que o da error (una x y fondo rojo suave) o se arranca
        - Arrancado: se pone el fondo en verde suave
            - Aparece el botón de stop para persistentes y el botón de papelera para los no persistentes.
                - El botó d'stop s'ha de mostrar o no depenent de la configuració
            - También deben aparecer tres botones de visores (spice, html5 y rdp, este en gris ya que aún no va) Lo ideal sería un botón con un drop-down y que al clicar se abre con el que marca pero que con el drop-down se puede cambiar a otro.
                - Debería pasarte para cada escritorio  una lista de visores disponibles y, si solo tiene uno ni mostrar el drop-down.
    - Gearbox a dalt amb paràmetres de configuració general

Endpoints

```
a.Mux.HandleFunc("/api/"+version+"/templates", a.isAuthenticated(a.templates))
a.Mux.HandleFunc("/api/"+version+"/create", a.isAuthenticated(a.create))
a.Mux.HandleFunc("/api/"+version+"/viewer", a.isAuthenticated(a.viewer))
a.Mux.HandleFunc("/api/"+version+"/desktops", a.isAuthenticated(a.desktops))
a.Mux.HandleFunc("/api/"+version+"/desktop/start", a.isAuthenticated(a.desktops))
a.Mux.HandleFunc("/api/"+version+"/desktop/stop", a.isAuthenticated(a.desktops))
```

Data structure

Nota: Simó: No crec que Templates hagi de tenir Viewers i State, crec que seria millor que Desktops tingues un valor de temporal o permanent i l'id del Template

```
type userTemplateRsp struct {
  ID   string json:"id,omitempty"
  ID_desktop string json:"id_desktop,omitempty"
  Name string json:"name,omitempty"
  Viewers [] json...
  State string json:"state,omitempty"
  Icon string json:"icon,omitempty"
}
type userDesktopRsp struct {
  ID   string json:"id,omitempty"
  Name string json:"name,omitempty"
  State string json:"state,omitempty"     // Started or Stopped
  Viewers array []                        // List of viewers
  Icon string json:"icon,omitempty"
}
```

sim6 proposal:

data structure:

- demplates:
    - id: template id
    - name: template name
    - icon: template icon
- desktops:
    - id: desktop id
    - name: desktop name
    - state: started/stopped
    - viewers: spice/html5/rdp
    - icon: desktop icon
    - tempalte: template id from its created
    - type: persistent/temporary

pseudo code of frontend

persistent desktops
```
for desktop in desktops:
  if desktop.type == persistent:
    print persistent button
    if desktop.state == started:
       print buttons stop and viewers
```

temporary desktops
```
for template in templates:
  print template button
  for desktop in desktops:
    if template.id == desktop.template and desktop.type == temporary:
      print started template and buttons
```
